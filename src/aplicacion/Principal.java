package aplicacion;

import java.util.Scanner;
import java.util.Random;
import java.util.InputMismatchException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;

public class Principal{
	public static void main(String args[]){
			Scanner sc = new Scanner(System.in);
			Scanner num = new Scanner(System.in);
			Scanner secreto = new Scanner(System.in);
			Scanner terminar = new Scanner(System.in);
			String usuario;
			int contador = 0;
			int NumeroGenerado = 0;
			int NumeroMaximo;
			int IntentoUsuario;
			int Seguir = 1;
do{

	do{
			System.out.println("\n\n			ADIVINO DE NUMEROS\n\n En este juego tendra que adivinar un numero secreto\n\n Para continuar introduzca su nombre de usuario: "); // Extra numero 1, pedir  nombre de usuario

				usuario = sc.nextLine(); 

					System.out.println("De acuerdo " + usuario +", a continuacion introduce un numero, este sera el limite en el que se encontrara el numero secreto, es decir, el numero estara entre 1 y el numero que introduzca ahora: ");
						do{
							NumeroMaximo = num.nextInt();

							if(NumeroMaximo <= 2 ) {System.out.println("El numero introducido tiene que ser mayor o igual a 2, vuelva a intentarlo"); }	//Extra numero 2
						}while(NumeroMaximo <= 2);
					
					NumeroGenerado = (int) (Math.random() * (NumeroMaximo + 1) ); //que genere un numero aleatorio entre  el numero maximo y 1
					
					do{
						contador ++;

							System.out.println("\nEl numero que ha de adivinar se encuentra entre 1 y " + NumeroMaximo + ", ahora intente adivinar el numero secreto: ");
							do{
								IntentoUsuario = secreto.nextInt();

								
								if(IntentoUsuario > NumeroMaximo){System.out.println("El numero que ha introducido no se encuentra entre 1 y " + NumeroMaximo + ",introduzca un numero que se encuentre entre ellos:");} //Extra Numero 3
							}while(IntentoUsuario > NumeroMaximo);

									{System.out.println("\n No ha acertado, intentelo otra vez "); }
						
						} while (IntentoUsuario != NumeroGenerado);
			
	}	while( IntentoUsuario != NumeroGenerado);

		System.out.println("\n\n ¡Enhorabuena "+ usuario + ", el numero secreto era el "+ NumeroGenerado + " lo ha adivinado en su intento numero " + contador + "\n\n");

			System.out.println("Si desea volver a jugar intoduzca 1 por teclado, si desea terminar de jugar, introduzca 2 ");

			Seguir = terminar.nextInt();
	
}while(Seguir == 1);
	}
}